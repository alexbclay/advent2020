use super::Solver;

pub struct DayTwentyFiveSolver {
    door_public: usize,
    door_private: Option<usize>,
    card_public: usize,
    card_private: Option<usize>,
}

fn find_loops(target: usize) -> Option<usize> {
    let mut cur_val = 1;
    let mut iter = 0;
    loop {
        if cur_val == target {
            return Some(iter);
        }
        cur_val = (cur_val * 7) % 20201227;
        iter += 1;
    }
    None
}

fn transform(value: usize, subject: usize, loop_size: usize) -> usize {
    let mut cur_val = value;
    for _ in 0..loop_size {
        cur_val = (cur_val * subject) % 20201227;
    }
    cur_val
}

impl Solver for DayTwentyFiveSolver {
    fn from_input(input: &String) -> Result<Box<DayTwentyFiveSolver>, String> {
        let mut lines = input.lines();
        let solver = DayTwentyFiveSolver {
            door_public: lines.next().unwrap().parse().expect("BAD INPUT"),
            card_public: lines.next().unwrap().parse().expect("BAD INPUT"),
            door_private: None,
            card_private: None,
        };

        Ok(Box::new(solver))
    }
    fn part_one(&self) -> Result<usize, &str> {
        let card_loops = find_loops(self.card_public).unwrap();
        let door_loops = find_loops(self.door_public).unwrap();
        println!("card loops:  {}  door loops: {}", card_loops, door_loops);

        println!("C -> D: {}", transform(1, self.card_public, door_loops));
        println!("D -> C: {}", transform(1, self.door_public, card_loops));
        Ok(transform(1, self.card_public, door_loops))
    }

    fn part_two(&self) -> Result<usize, &str> {
        Ok(2)
    }
}
